## Gitblit Powertools plugin

## 1.0.1

- Added repository fork command
- Completed repository field setting

## 1.0.0

- Initial release
